import React from "react"
import Layout from "../components/layout"
import "../components/layout.css"
import Homepage1 from "../components/homepage/homepage-1"
import Homepage2 from "../components/homepage/homepage-2"

class Homepage extends React.Component {
    render() {
        return (
            <Layout>
                <Homepage1 />
                <Homepage2 />
            </Layout>
        )
    }
}

export default Homepage
