import React from "react"
import "../components/layout.css"
import Layout from "../components/layout"
import { ApolloProvider } from "@apollo/react-hooks"
import Client from "../components/client"
import ContactForm from "../components/contact/contactForm"
import { Mutation } from "react-apollo"
import AddContactGql from "../components/contact/addContactGql"
import { Alert } from "rsuite"

class Contact extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            contact: {
                name: null,
                email: null,
                phone: null,
                society: null,
                message: null,
            },
        }
    }

    render() {
        return (
            <ApolloProvider client={Client}>
                <Layout>
                    <div className="background-contact" />
                    <Mutation
                        mutation={AddContactGql}
                        onError={error => {
                            Alert.error("Network Down")
                        }}
                        onCompleted={data => {
                            Alert.success(
                                "We appreciate your interest. We will get back to you soon. :)"
                            )
                        }}
                    >
                        {(insertContact, { loading, error }) => (
                            <ContactForm
                                insertContact={insertContact}
                                error={error}
                                loading={loading}
                            />
                        )}
                    </Mutation>
                </Layout>
            </ApolloProvider>
        )
    }
}

export default Contact
