import React from "react"
import "../components/layout.css"
import Layout from "../components/layout"
import SocietyContainer from "../components/society/societyContainer"
import ContainerWithHeaderOnly from "../components/containerWithHeaderOnly"
import { Parallax } from "react-scroll-parallax"

class Society extends React.Component {
    render() {
        return (
            <Layout>
                <Parallax className="background-society" />
                <div style={{ marginTop: `-10%` }}>
                    <ContainerWithHeaderOnly>
                        <SocietyContainer />
                    </ContainerWithHeaderOnly>
                </div>
            </Layout>
        )
    }
}

export default Society
