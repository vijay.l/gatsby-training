import React from "react"
import "../components/layout.css"
import Layout from "../components/layout"
import RenderFlexbox from "../components/references/renderFlexbox"
import ReferenceList from "../components/references/referenceList"
import ContainerWithHeaderOnly from "../components/containerWithHeaderOnly"

class References extends React.Component {
    render() {
        return (
            <Layout>
                <div style={{ height: `20rem`, width: `100%` }} />
                <ContainerWithHeaderOnly className="references">
                    <img
                        alt=""
                        src="https://www.surya-digital.com/wp-content/uploads/2017/10/slall-heading.png"
                    />
                    <h1 style={{ marginTop: `-1px` }}>
                        accompanies the success of its customers
                    </h1>
                    <div className="references-left-div">
                        <h3 style={{ textAlign: `left` }}>
                            We build lasting relationships with each of our
                            clients.
                        </h3>
                        <RenderFlexbox />
                    </div>
                    <div className="references-right-div">
                        <h3>
                            Our long-term partnership policy contributes to your
                            growth in various business sectors.
                        </h3>
                        <ReferenceList />
                    </div>
                    <h1 className="references-lower-header">
                        Contact us to know our references and deepen our areas
                        of expertise.
                    </h1>
                </ContainerWithHeaderOnly>
            </Layout>
        )
    }
}

export default References
