import React from "react"
import "../layout.css"

const RenderFlexbox = () => {
    return (
        <div className="references-flexbox">
            <div className="references-flexbox-item">
                <img
                    alt=""
                    src="https://www.surya-digital.com/wp-content/uploads/2017/10/ref_1.png"
                    style={{ marginBottom: `10px` }}
                />
                <p>Flexibility</p>
            </div>

            <div className="references-flexbox-item">
                <img
                    alt=""
                    src="https://www.surya-digital.com/wp-content/uploads/2017/10/ref_2.png"
                    style={{ marginBottom: `10px` }}
                />
                <p>Reactivity</p>
            </div>

            <div className="references-flexbox-item">
                <img
                    alt=""
                    src="https://www.surya-digital.com/wp-content/uploads/2017/10/ref_3.png"
                    style={{ marginBottom: `10px` }}
                />
                <p>Adaptability</p>
            </div>

            <div className="references-flexbox-item">
                <img
                    alt=""
                    src="https://www.surya-digital.com/wp-content/uploads/2017/10/ref_4.png"
                    style={{ marginBottom: `10px` }}
                />
                <p>Transparency</p>
            </div>
        </div>
    )
}

export default RenderFlexbox
