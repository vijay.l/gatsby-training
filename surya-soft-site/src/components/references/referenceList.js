import React from "react"
import "../layout.css"

const ReferenceList = () => {
    return (
        <ul style={{ listStyle: "none" }}>
            <li> > Major French Luxury Brands</li>
            <li> > International Banks</li>
            <li> > Communication Agencies</li>
            <li> > Startups</li>
            <li> > Multinationals</li>
        </ul>
    )
}

export default ReferenceList
