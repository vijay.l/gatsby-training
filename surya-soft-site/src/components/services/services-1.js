import React from "react"
import Container from "../container"
import FeatureContainer from "../featureContainer"
import "../layout.css"
import { Parallax } from "react-scroll-parallax"

class Services1 extends React.Component {
    render() {
        return (
            <div>
                <Parallax className="background-image1" />
                <Container
                    containerTitle="Testing Activity"
                    excerpt="Offer an optimal digital experience to your customers"
                >
                    <div className="feature-container-div">
                        <FeatureContainer
                            headerIcon="https://www.surya-digital.com/wp-content/themes/surya-digital/images/service_icon_1.png"
                            excerpt="Adapt your IT products whatever the support, the operating system or the browser, for your Internet users."
                        />
                        <FeatureContainer
                            headerIcon="https://www.surya-digital.com/wp-content/themes/surya-digital/images/service_icon_2.png"
                            excerpt="The number of tests to perform is exponential before the release of your releases."
                        />
                        <FeatureContainer
                            headerIcon="https://www.surya-digital.com/wp-content/themes/surya-digital/images/service_icon_3.png"
                            excerpt="We make them for you on more than 70 platforms. 20 to 30% of the production time is saved."
                        />
                        <FeatureContainer
                            headerIcon="https://www.surya-digital.com/wp-content/themes/surya-digital/images/service_icon_4.png"
                            excerpt="We make them for you on more than 70 platforms. 20 to 30% of the production time is saved."
                        />
                    </div>
                    <h1>2 Processes</h1>
                    <div className="services1-processes">
                        <div className="services1-processes-header">
                            <img
                                className="services1-processes-image"
                                src="https://www.surya-digital.com/wp-content/uploads/2017/10/surya_1.png"
                                alt="Your site or application is already existing."
                            />
                            <h3>
                                Your site or application is already existing.
                            </h3>
                        </div>
                        <h4>
                            We perform comprehensive, technical and ergonomic
                            testing of your digital products. We analyze and
                            identify dysfunctions. Together we find the right
                            solutions.
                        </h4>
                    </div>
                    <div className="services1-processes">
                        <div className="services1-processes-header">
                            <img
                                className="services1-processes-image"
                                src="https://www.surya-digital.com/wp-content/uploads/2017/10/surya_2.png"
                                alt="Your site or application is already existing."
                            />
                            <h3>
                                Your site or application is in the development
                                phase
                            </h3>
                        </div>
                        <h4>
                            We offer direct integration into your production
                            process. The tests are carried out throughout the
                            design. Our engineers solve anomalies as soon as
                            they are diagnosed.
                        </h4>
                    </div>
                    <h2 className="services-header-lower">
                        Our experts fix the most common bugs, without touching
                        the features. We guarantee a Quality Assurance (QA)
                        compliant.
                    </h2>
                </Container>
            </div>
        )
    }
}

export default Services1
