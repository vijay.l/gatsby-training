import React from "react"
import Container from "../container"
import { Parallax } from "react-scroll-parallax"

class Services3 extends React.Component {
    render() {
        return (
            <div>
                <Parallax className="background-image3" />
                <Container excerpt="Get relevant data and increase your performance.">
                    <div className="services3">
                        <div style={{ maxWidth: `50%` }}>
                            <h1 style={{ marginBottom: `30px` }}>
                                Data Scientists
                            </h1>
                            <h4>
                                The Bangalore University of Science and
                                Technology is one of the most advanced centers
                                in the processing and analysis of big data.
                            </h4>
                            <h4>
                                The best PHDs in the field are trained in this
                                institution, and the biggest players in big data
                                are there.
                            </h4>
                            <h4>
                                In partnership with the University of Bangalore,
                                we set up centers of expertise specialized in
                                your sector.
                            </h4>
                            <h4>
                                Our doctoral students in mathematics,
                                statistics, or specialized in other
                                technologies, formulate advanced algorithms. By
                                precisely defining your search, our algorithms
                                make it possible to extract the data generating
                                your growth.
                            </h4>
                        </div>
                        <div className="services3-image-div">
                            <img
                                className="sevices3-image "
                                src="https://www.surya-digital.com/wp-content/uploads/2017/10/data-img.png"
                                alt=""
                            />
                        </div>
                    </div>
                </Container>
            </div>
        )
    }
}

export default Services3
