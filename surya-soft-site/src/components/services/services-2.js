import React from "react"
import Container from "../container"
import FeatureContainer from "../featureContainer"
import { Parallax } from "react-scroll-parallax"

class Services2 extends React.Component {
    render() {
        return (
            <div>
                <Parallax className="background-image2" />
                <Container
                    containerTitle="IOS & Android Apps Development"
                    excerpt="Choose the experience to make your Apps."
                >
                    <FeatureContainer
                        headerIcon="https://www.surya-digital.com/wp-content/uploads/2017/10/star_1.png"
                        excerpt="Create quality apps for your users. "
                    />
                    <FeatureContainer
                        headerIcon="https://www.surya-digital.com/wp-content/uploads/2017/10/star_2.png"
                        excerpt="The development of Apps on iOS is provided by a team of engineers who worked for Apple."
                    />
                    <FeatureContainer
                        headerIcon="https://www.surya-digital.com/wp-content/uploads/2017/10/star_3.png"
                        excerpt="Our technicians use all existing programming languages ​​on Android."
                    />
                    <h2 className="services-header-lower">
                        From design to production, our developers and
                        technicians produce software guaranteed to Quality
                        Assurance (QA) standards.
                    </h2>
                </Container>
            </div>
        )
    }
}

export default Services2
