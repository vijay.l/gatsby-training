import React from "react"
import ContainerWithHeaderOnly from "../containerWithHeaderOnly"
import { Parallax } from "react-scroll-parallax"

class Services4 extends React.Component {
    render() {
        return (
            <div>
                <Parallax className="background-image4" />
                <ContainerWithHeaderOnly excerpt="Boost your business performance.">
                    <div className="services4">
                        <h1 style={{ marginBottom: `30px` }}>
                            Dynamics AX Competence Center
                        </h1>
                        <h4>
                            Take advantage of our 150 engineers to optimize the
                            use of Microsoft Dynamics AX.
                        </h4>
                        <ul style={{ listStyle: "none" }}>
                            <li>
                                > Specific development adapted to the needs of
                                your company.
                            </li>
                            <li>
                                > Consulting-parameter setting to make the most
                                of the capabilities of your ERP.
                            </li>
                            <li>> From data migration to your new platform.</li>
                            <li>
                                > Data collection and analysis to help you with
                                your decision strategy.
                            </li>
                        </ul>
                        <h4>
                            Well used, this package reduces the risk of error
                            and becomes a major asset of your success.
                        </h4>
                    </div>
                    <div className="services4-image-div">
                        <img
                            alt=""
                            src="https://www.surya-digital.com/wp-content/uploads/2017/10/microsoft_dynamic.png"
                        />
                    </div>
                </ContainerWithHeaderOnly>
            </div>
        )
    }
}

export default Services4
