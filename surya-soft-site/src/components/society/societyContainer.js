import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import "../layout.css"

var colors = ["#181818", "#505050", "#282828"]
export default () => {
    var count = 0
    const data = useStaticQuery(
        graphql`
            query {
                allMarkdownRemark {
                    edges {
                        node {
                            excerpt(pruneLength: 1200, format: HTML)
                            frontmatter {
                                title
                            }
                        }
                    }
                }
            }
        `
    )
    return (
        <div className="society-container">
            <div className="card-div">
                {data.allMarkdownRemark.edges.map(({ node }) => (
                    <div
                        key={node.id}
                        className="card"
                        style={{
                            backgroundColor: colors[count],
                            float: count === 1 ? `right` : `left`,
                        }}
                        onAnimationStart={count++}
                    >
                        <h1 style={{ width: `80%` }}>
                            {node.frontmatter.title}
                        </h1>
                        <p
                            dangerouslySetInnerHTML={{ __html: node.excerpt }}
                        ></p>
                    </div>
                ))}
            </div>
        </div>
    )
}
