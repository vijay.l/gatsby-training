---
title: An international multi-skills board
---

Made up in particular of Sreekanth Mangulam, historical co-chairman, the members of the board of directors are international, with diversified and global specialties.

Didier Pichon, business partner and longtime friend of DN Prahlad, joins the board in 2017; Former general manager of Chanel's information systems, he wants to increase the international dimension of Surya.

Together they define a new business development strategy.

Present in luxury, distribution, logistics, production and banking, they explore new markets, to create adapted services.

[OUR REFERENCES](/references)
