---
title: In the beginning,a know how !
---

Surya Software Systems Private Limited is a service company based in India, established in 1999 by DN Prahlad, Senior Executive Officer and Member of the Board of Infosys.

Originally the main activity is the development of financial security software for trading rooms, small and medium-sized banks.

As visionaries on the evolution of emerging markets, our engineers are increasing the offer of services around digital tools.

In a few years, Surya stands out as the solution for a better digital performance of companies.

[OUR SERVICES](/services)
