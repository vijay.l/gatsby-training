import { gql } from "apollo-boost"

const AddContactGql = gql`
    mutation insertContact(
        $name: String
        $email: String
        $phone: String
        $society: String
        $message: String
    ) {
        insert_contact(
            objects: [
                {
                    name: $name
                    email: $email
                    phone: $phone
                    society: $society
                    message: $message
                }
            ]
        ) {
            returning {
                name
            }
        }
    }
`

export default AddContactGql
