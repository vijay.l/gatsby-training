import React from "react"
import {
    Form,
    FormControl,
    ButtonToolbar,
    Button,
    ControlLabel,
    FormGroup,
    Alert,
} from "rsuite"
import ContainerWithHeaderOnly from "../containerWithHeaderOnly"
import "../layout.css"
import "rsuite/dist/styles/rsuite.min.css"
import { ContactFormStates } from "../types/index"

class ContactForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            contact: {
                name: null,
                email: null,
                message: null,
                society: null,
                phone: null,
            },
        }
    }

    handleChange = (eventName, eventValue) => {
        this.setState(state => {
            state.contact[eventName] = eventValue
        })
    }

    handleSubmit = event => {
        event.preventDefault()
        debugger
        if (
            !this.state.contact.name ||
            !this.state.contact.email ||
            !this.state.contact.phone ||
            !this.state.contact.society
        ) {
            Alert.error(
                "One or more mandatory fields empty! Please provide us with full information so we can revert back as soon as possible. :)",
                5000
            )
        }
        if (
            this.state.contact.name &&
            this.state.contact.email &&
            this.state.contact.phone &&
            this.state.contact.society
        ) {
            this.props.insertContact({
                variables: {
                    name: this.state.contact.name,
                    email: this.state.contact.email,
                    phone: this.state.contact.phone,
                    message: this.state.contact.message,
                    society: this.state.contact.society,
                },
            })
        }
    }

    renderFormGroup = name => {
        var formName = name === "Message" ? "Your Message" : name + "*"
        return (
            <FormGroup>
                <ControlLabel>{formName} :</ControlLabel>
                <FormControl
                    className="contact-form-control"
                    name="name"
                    onChange={event => {
                        this.handleChange(name.toLowerCase(), event)
                    }}
                ></FormControl>
            </FormGroup>
        )
    }

    render() {
        return (
            <ContainerWithHeaderOnly excerpt="Contact Us">
                <div className="form">
                    <Form
                        fluid
                        onSubmit={event => {
                            this.handleSubmit(event)
                        }}
                        formValue={this.state.contact.value}
                    >
                        {this.renderFormGroup("Name")}
                        {this.renderFormGroup("Email")}
                        {this.renderFormGroup("Phone")}
                        {this.renderFormGroup("Society")}
                        {this.renderFormGroup("Message")}
                        <FormGroup>
                            <ButtonToolbar>
                                <Button appearance="ghost" type="submit">
                                    Submit
                                </Button>
                            </ButtonToolbar>
                        </FormGroup>
                    </Form>
                </div>
            </ContainerWithHeaderOnly>
        )
    }
}

export default ContactForm
