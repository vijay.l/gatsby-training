import React from "react"
import PropTypes from "prop-types"
import "./layout.css"

const Excerpt = ({ excerpt }) => {
    return (
        <div className="image-excerpt">
            <div>
                <h1>{excerpt}</h1>
            </div>
        </div>
    )
}

Excerpt.prototype = {
    excerpt: PropTypes.string,
}

export default Excerpt
