export interface ContactFormStates {
    name: string
    email: string
    phone: string
    society: string
    message: string
}
