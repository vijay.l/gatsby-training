import React from "react"
import PropTypes from "prop-types"
import "./layout.css"
import { Link } from "gatsby"
import { Container, Sidebar, Content, Nav, Navbar } from "rsuite"
import "../components/layout.css"
import { navigate } from "gatsby"
import { ParallaxProvider } from "react-scroll-parallax"

const Layout = ({ children }) => {
    return (
        <ParallaxProvider>
            <div style={{ width: `100%` }}>
                <Navbar
                    className="layout-header"
                    style={{ background: `transparent` }}
                >
                    <Navbar.Header>
                        <Link to="/">
                            <div className="layout-header-logo-shadow" />
                            <img
                                src="https://www.surya-digital.com/wp-content/themes/surya-digital/images/logo.png"
                                style={{ opacity: `0.9` }}
                                alt=""
                            />
                        </Link>
                    </Navbar.Header>
                    <Container
                        style={{
                            display: `block`,
                        }}
                    >
                        <Navbar.Body className="navbar">
                            <Nav>
                                <Nav.Item
                                    className="navbar-item"
                                    onClick={event => {
                                        navigate("/society")
                                    }}
                                >
                                    <Link to="/society">Society</Link>
                                </Nav.Item>
                                <Nav.Item
                                    className="navbar-item"
                                    onClick={event => {
                                        navigate("/services")
                                    }}
                                >
                                    <Link to="/services">Services</Link>
                                </Nav.Item>
                                <Nav.Item
                                    className="navbar-item"
                                    onClick={event => {
                                        navigate("/references")
                                    }}
                                >
                                    <Link to="/references">References</Link>
                                </Nav.Item>
                                <Nav.Item
                                    className="navbar-item"
                                    onClick={event => {
                                        navigate("/contact")
                                    }}
                                >
                                    <Link to="/contact">Contact</Link>
                                </Nav.Item>
                            </Nav>
                        </Navbar.Body>
                    </Container>
                </Navbar>
                <main>{children}</main>
                <Container className="layout-footer">
                    <Container>
                        <Sidebar className="layout-footer-logo" />
                        <Content>
                            <p className="legal-notice-p">Legal Notice</p>
                            <div className="layout-footer-links">
                                <Content>
                                    <Link to="/society">Society</Link>
                                </Content>
                                <Content>
                                    <Link to="/services">Services</Link>
                                </Content>
                                <Content>
                                    <Link to="/references">References</Link>
                                </Content>
                                <Content>
                                    <Link to="/contact">Contact</Link>
                                </Content>
                            </div>
                        </Content>
                    </Container>
                </Container>
            </div>
        </ParallaxProvider>
    )
}

Layout.propTypes = {
    children: PropTypes.node.isRequired,
}

export default Layout
