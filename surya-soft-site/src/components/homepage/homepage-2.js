import React from "react"
import "../layout.css"
import "rsuite/dist/styles/rsuite.min.css"
import { Parallax } from "react-scroll-parallax"

class Homepage2 extends React.Component {
    render() {
        return (
            <div>
                <Parallax className="background-homepage2" />
                <div className="homepage2">
                    <h1>18 years of experience at your service .</h1>
                    <h3 className="homepage2-p">
                        Established on 4 continents and 20 countries, we favor
                        the long-term partnership with our customers, to promote
                        your brand image optimally.
                    </h3>
                </div>
            </div>
        )
    }
}

export default Homepage2
