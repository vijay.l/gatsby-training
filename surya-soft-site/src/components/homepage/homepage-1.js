import React from "react"
import Container from "../container"
import FeatureContainer from "../featureContainer"
import "../layout.css"
import { Button, ButtonToolbar } from "rsuite"
import "rsuite/dist/styles/rsuite.min.css"
import { navigate } from "gatsby"
import { Parallax } from "react-scroll-parallax"

class Homepage1 extends React.Component {
    render() {
        return (
            <div>
                <Parallax className="background-homepage1" />
                <Container
                    excerpt="Adapt your 
                        Information Technologies (IT) 
                        to the vision of your brand"
                >
                    <div>
                        <img
                            src="https://www.surya-digital.com/wp-content/uploads/2017/10/slall-heading.png"
                            alt=""
                        />
                        <h1 style={{ marginTop: `-0.01rem` }}>
                            accelerate your growth
                        </h1>
                    </div>
                    <div>
                        <FeatureContainer
                            headerIcon="https://www.surya-digital.com/wp-content/themes/surya-digital/images/icon1.png"
                            excerpt="Benefit from the experience of the greatest data scientists."
                        />
                        <FeatureContainer
                            headerIcon="https://www.surya-digital.com/wp-content/themes/surya-digital/images/icon2.png"
                            excerpt="Give your customers the best use of your digital tools."
                        />
                        <FeatureContainer
                            headerIcon="https://www.surya-digital.com/wp-content/themes/surya-digital/images/icon3.png"
                            excerpt="Coordinate the IT management devices of your business."
                        />
                        <FeatureContainer
                            headerIcon="https://www.surya-digital.com/wp-content/themes/surya-digital/images/icon3.png"
                            excerpt="And increase your value creation."
                        />
                    </div>
                    <ButtonToolbar className="button-toolbar">
                        <Button
                            appearance="ghost"
                            className="buton"
                            style={{ background: `black` }}
                            onClick={event => {
                                navigate("/services")
                            }}
                        >
                            See Services
                        </Button>
                    </ButtonToolbar>
                    <h2 className="homepage1-header-mid">
                        We establish together, centers of competence, dedicated
                        to the optimization of your Information Systems (IS).
                    </h2>
                    <h1 className="homepage1-concentrate">Concentrate</h1>
                    <h1>on your know how</h1>
                    <div>
                        <FeatureContainer
                            headerIcon="https://www.surya-digital.com/wp-content/themes/surya-digital/images/watch_icon.png"
                            excerpt="Speed ​​up your internal process."
                        />
                        <FeatureContainer
                            headerIcon="https://www.surya-digital.com/wp-content/themes/surya-digital/images/icon5.png"
                            excerpt="We directly integrate your release development teams."
                        />
                        <FeatureContainer
                            headerIcon="https://www.surya-digital.com/wp-content/themes/surya-digital/images/icon6.png"
                            excerpt="Our proven test methodologies, list the malfunctions of your sites and applications, to establish a quick and adapted response."
                        />
                    </div>
                    <ButtonToolbar className="button-toolbar">
                        <Button
                            appearance="ghost"
                            className="buton"
                            style={{ background: `black` }}
                            onClick={event => {
                                navigate("/references")
                            }}
                        >
                            See Our References
                        </Button>
                    </ButtonToolbar>
                    <h1>
                        Unobstructed from these tasks, your designers can focus
                        on innovation.
                    </h1>
                </Container>
            </div>
        )
    }
}

export default Homepage1
