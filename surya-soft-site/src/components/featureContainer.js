import React from "react"
import PropTypes from "prop-types"
import "./layout.css"

const FeatureContainer = ({ headerIcon, excerpt, children, alts }) => {
    return (
        <div className="feature-container">
            {children}
            <img
                className="feature-container-image"
                src={headerIcon}
                alt={alts}
            />
            <h4>{excerpt}</h4>
        </div>
    )
}

FeatureContainer.prototype = {
    headerIcon: PropTypes.string,
    excerpt: PropTypes.string,
    children: PropTypes.node.isRequired,
    alts: PropTypes.string,
}

export default FeatureContainer
