import React from "react"
import PropTypes from "prop-types"
import "./layout.css"
// import {ParallaxLayer} from "react-spring/renderprops-addons"
// import { Parallax, Background } from 'react-parallax';
import { Parallax } from "react-scroll-parallax"
import Excerpt from "./excerpt"

const Container = ({ containerTitle, children, excerpt }) => {
    return (
        <Parallax
            y={[-2, 5]}
            tagOuter="figure"
            styleOuter={{ width: `100%`, marginLeft: `0px` }}
        >
            <Excerpt excerpt={excerpt} />
            <img
                className="container-header"
                src="https://www.surya-digital.com/wp-content/themes/surya-digital/images/tilt_border.png"
                alt=""
                style={{ width: `100%` }}
            />
            <div className="container">
                <h1>{containerTitle}</h1>
                <div>{children}</div>
                <img
                    className="container-footer"
                    src="https://www.surya-digital.com/wp-content/themes/surya-digital/images/tilt_border_footer.png"
                    alt=""
                />
            </div>
        </Parallax>
    )
}

Container.prototype = {
    containerTitle: PropTypes.string,
    children: PropTypes.node.isRequired,
    excerpt: PropTypes.string,
}

export default Container
