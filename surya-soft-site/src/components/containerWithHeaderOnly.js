import React from "react"
import PropTypes from "prop-types"
import "./layout.css"
import { Parallax } from "react-scroll-parallax"
import Excerpt from "./excerpt"

const ContainerWithHeaderOnly = ({
    containerTitle,
    children,
    excerpt,
    className,
}) => {
    return (
        <Parallax className={className}>
            <Excerpt excerpt={excerpt} />
            <img
                className="container-header"
                src="https://www.surya-digital.com/wp-content/themes/surya-digital/images/tilt_border.png"
                alt=""
            />
            <div className="container" style={{ minHeight: `600px` }}>
                <h1>{containerTitle}</h1>
                <div>{children}</div>
            </div>
        </Parallax>
    )
}

ContainerWithHeaderOnly.prototype = {
    containerTitle: PropTypes.string,
    children: PropTypes.node.isRequired,
    excerpt: PropTypes.string,
    className: PropTypes.string,
}

export default ContainerWithHeaderOnly
